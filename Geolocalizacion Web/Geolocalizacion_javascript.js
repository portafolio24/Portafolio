﻿//Implementar evento que cargue al inicio de la página
//Cuando ocurre el evento, cómo se llama y qué espera del usuario
window.addEventListener("load", comenzar, false);

//Crear función "comenzar" que es la que llamará al evento addEventListener
function comenzar() {
    //Variable para obtener el evento del botón
        //El parámetro que recibe es el id del elemento
    var miBoton = document.getElementById("btnUbicacion");
    //El botón espera el evento click
    miBoton.addEventListener("click", obtener, false);
}

function obtener() {
    //Método GetCurrentPosition
        //Se va a ejecutar cuando la geolocalización tenga éxito y va a mandar llamar la función parámetro
        //Es como un if(si se obtiene la ubicación){llama la función}
    navigator.geolocation.getCurrentPosition(mostrar_posicion, errores);
}


//Parametro que devuelve el getCurrentPosition en caso de si poder encontrar una ubicación
function mostrar_posicion(posicion) {
    var ubicacion = document.getElementById("ubicacion");
    var ubicacionReal = " ";
    ubicacionReal = "Latitud: " + posicion.coords.latitude + "<br>"; //Se extrae de toda la información de cada parámetro solicitado
    ubicacionReal += "Longitud: " + posicion.coords.longitude + "<br>";
    ubicacionReal += "Exactitud: " + posicion.coords.accuracy;
    ubicacion.innerHTML = ubicacionReal; //Indica dónde se va a poner la información, entonces lo encontrado lo va a poner en el section de HTML
}

function errores(error) {
    //alert("Ocurrió un error" + " " + error.code + " " + error.message);
    if (error.code == 1) {
        alert("Error, se debe permitir la geolocalización en tu navegador.");
    }
    if (error.code == 2) {
        alert("La ubicación no se encuentra disponible en este momento.");
    }
    if (error.code == 3) {
        alert("El tiempo de espera para obtener la geolocalización ha sido excedido.");
    }
}