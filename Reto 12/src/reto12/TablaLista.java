package reto12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import javax.swing.JFrame;
import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.event.*;
import javax.swing.table.TableModel;


/**
 * 
 * @author Emma Arias
 */
public class TablaLista implements TableModelListener{
    public static Connection getConnection(){
        Connection connection = null;
        String connectUrl = "jdbc:sqlserver://reto12.database.windows.net:1433;"+
                                "database=SistemaEscolar;"+
                                "user=usuarioazure@reto12;"+
                                "password={Tecmilenio.182};"+"encrypt=true;"+
                                "trustServerCertificate=false;"+
                                "hostNameInCertificate=*.database.windows.net;"+
                                "loginTimeout=30;";
        try{
            connection = DriverManager.getConnection(connectUrl);
        }catch(Exception e){}
        
        return connection;
}
    public static ResultSet getTabla(String consultaSql){
        Connection connect = getConnection();
        Statement st;
        ResultSet resultSet = null;
        try{
            st = connect.createStatement();
            resultSet = st.executeQuery(consultaSql);
        } catch(Exception e){
        
        }
        return resultSet;
    
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int row = e.getFirstRow();
        int column = e.getColumn();
        TableModel tabla = (TableModel) e.getSource();
        String columnName = tabla.getColumnName(column);
        Object data = tabla.getValueAt(row, column);
        PantallaPrincipal pp = new PantallaPrincipal();
        pp.imprimir(row);
    }
    
    
}
