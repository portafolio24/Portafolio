package reto12;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static reto12.TablaLista.getConnection;


public class MetodoSql {
    public void Registro(String nombre, String apellido){
        String connectionUrl = "jdbc:sqlserver://reto12.database.windows.net:1433;"+
                                "database=SistemaEscolar;"+
                                "user=usuarioazure@reto12;"+
                                "password={Tecmilenio.182};"+"encrypt=true;"+
                                "trustServerCertificate=false;"+
                                "hostNameInCertificate=*.database.windows.net;"+
                                "loginTimeout=30;";
        
        String insertSql = "INSERT INTO dbo.listaEstudiantes (nombre, apellido) VALUES ('"+nombre+"','"+apellido+"')";
        ResultSet resultSet = null;
        
        try(Connection connection = DriverManager.getConnection(connectionUrl);
                PreparedStatement prepInsert = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);){
            prepInsert.execute();
            resultSet = prepInsert.getGeneratedKeys();
            
            while(resultSet.next()){
                System.out.println("Generado:"+resultSet.getString(1));
            }
            JOptionPane.showMessageDialog(null, "Operación Exitosa", "Se ha registrado el alumno con éxito", JOptionPane.INFORMATION_MESSAGE);
            
        } catch(Exception e){
            e.printStackTrace();
        }
        PantallaPrincipal pp = new PantallaPrincipal();
        pp.lista();
    }
    public void Eliminar(int id){
        System.out.println(id);
        Connection connect = getConnection();
        int id1 = id;
        try{
        PreparedStatement preparedStatement = connect.prepareStatement("DELETE FROM dbo.listaEstudiantes WHERE id="+id);
        //preparedStatement.setInt(1, id1);
        preparedStatement.execute();
        connect.close();
    } catch(Exception e){}
        JOptionPane.showMessageDialog(null, "Operación Exitosa", "Se ha eliminado el alumno", JOptionPane.INFORMATION_MESSAGE);
        
    }
}
