﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SitioTI42
{
    public partial class BajaAlumno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btEliminar_Click(object sender, EventArgs e)
        {
            int eadAlumno;
            eadAlumno = int.Parse(tbBaja.Text);

            //Crear el objeto para accesar al dataset
            utTableAdapters.AlumnosTableAdapter taID = new utTableAdapters.AlumnosTableAdapter();

            //Llamar através del objeto la función DELETE perteneciente al dataset
            taID.Delete(eadAlumno);

            Response.Redirect("BajaAlumno.aspx");

        }
    }
}