﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient; //Eventos para la BD

namespace SitioTI42
{
    public partial class Alumnos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
        {
            string SQLInsert;
            SqlConnection SQLCon = new SqlConnection("Data source=LAB10-PC14; Initial Catalog=ut42; Integrated Security=True");//Apertura para la BD
            SqlCommand SQLCmd = new SqlCommand();

            using (SQLCon) {
                SQLCon.Open(); //Abre la base de datos UT
                SQLCmd.Connection = SQLCon;

                //Declaración de variables para tomar la información ingresada por el usuario
                string eacNombres, eacAPaterno, eacAMaterno, eacCorreo;
                char eacSexo;
                int eacGrupo, eacCarrera;

                //Asignación de valores introducidos al formulario a su variable
                eacNombres = tbNombres.Text;
                eacAPaterno = tbPaterno.Text;
                eacAMaterno = tbMaterno.Text;
                eacCorreo = tbEmail.Text;

                eacSexo = char.Parse(rblSexo.Text);
                eacGrupo = int.Parse(ddlGrupo.Text);
                eacCarrera = int.Parse(ddlCarrera.Text);

                SQLInsert = String.Format("Insert into Alumnos(nombres,ap_paterno,ap_materno,sexo,email,id_grupo,id_carrera)"+
                    "Values('{0}','{1}','{2}','{3}','{4}',{5},{6})", eacNombres, eacAPaterno, eacAMaterno, eacSexo, eacCorreo, eacGrupo,eacCarrera);

                SQLCmd.CommandText = SQLInsert;
                SQLCmd.ExecuteNonQuery();

                //Manda mensaje para informar que se realizó exitosamente la operación
                string script = "alert('Alumno dado de alta correctamente');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);

                SQLCon.Close();

                tbEmail.Text = "";
                tbMaterno.Text = "";
                tbNombres.Text = "";
                tbPaterno.Text = "";
                
            } //Cierre del using
        }
    }
}