﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SitioTI42
{
    public partial class ModificacionAlumnno : System.Web.UI.Page
    {
        //Si no esta como constante, no llega al data adapter, porque no lo detectan
       public static int eacIdAlumno;
        //Variable global para usar en buscar y guardar.
        protected void Page_Load(object sender, EventArgs e)
        {
            tbDatos.Visible = false;
            tbBotones.Visible = false;
        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            if (tbIdAlumno.Text != "") {
                eacIdAlumno = int.Parse(tbIdAlumno.Text);

                utTableAdapters.AlumnosTableAdapter taAlumnos = new utTableAdapters.AlumnosTableAdapter();

                //Trae todo un registro en base al parámetro ingresado en la variable global
                ut.AlumnosDataTable dtAlumnos = taAlumnos.GetData(eacIdAlumno);

                //Contar el número de registros que regresó con el ID
                int totalRegistros = dtAlumnos.Count();

                if (totalRegistros > 0) {
                    ut.AlumnosRow RowRegistro = dtAlumnos[0];

                    //Devolver cada atributo a su campo correspondiente
                    tbNombres.Text = RowRegistro.nombres;
                    tbPaterno.Text = RowRegistro.ap_paterno;
                    tbMaterno.Text = RowRegistro.ap_materno;
                    tbEmail.Text = RowRegistro.email;
                    rblSexo.SelectedValue = RowRegistro.sexo;

                    //El row parsea el valor a cadena de caracteres ya que se necesita imprimir string
                    ddlCarrera.SelectedValue = RowRegistro.id_carrera.ToString();
                    ddlGrupo.SelectedValue = RowRegistro.id_grupo.ToString();

                    //Volver visible las tablas y formulario
                    tbDatos.Visible = true;
                    tbBotones.Visible = true;
                }
                else {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "javascript:alert('No se encontro el ID del Alumno a Modificar');", true);
                }
            }
            else {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "javascript:alert('Necesitas ingresar el ID a modificar');", true);
            }

        }

        protected void btGuardar_Click(object sender, ImageClickEventArgs e)
        {
            string eacNombres, eacAPaterno, eacAMaterno, eacCorreo;
            char eacSexo;
            int eacGrupo, eacCarrera;

            //Asignación de valores introducidos al formulario a su variable
            eacNombres = tbNombres.Text;
            eacAPaterno = tbPaterno.Text;
            eacAMaterno = tbMaterno.Text;
            eacCorreo = tbEmail.Text;

            eacSexo = char.Parse(rblSexo.Text);
            eacGrupo = int.Parse(ddlGrupo.Text);
            eacCarrera = int.Parse(ddlCarrera.Text);

            utTableAdapters.AlumnosTableAdapter taID = new utTableAdapters.AlumnosTableAdapter();

            taID.Update(eacNombres,eacAPaterno,eacAMaterno,eacSexo.ToString(),eacCorreo,eacGrupo,eacCarrera,eacIdAlumno);

            string script = "alert('Alumno modificado correctamente'); window.location = '" + Request.ApplicationPath + "ModificacionAlumnno.aspx';";

            ScriptManager.RegisterStartupScript(this, GetType(), "alerta", script, true);
        }
    }
}