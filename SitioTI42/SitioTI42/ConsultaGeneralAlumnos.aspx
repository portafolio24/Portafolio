﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsultaGeneralAlumnos.aspx.cs" Inherits="SitioTI42.ConsultaGeneralAlumnos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <!-- Seccion Metadatos -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="sitio informativo de la carrera de IDGS" />
    <meta name="author" content="Emma Mariana Arias Camacho" />
    <meta name="keywords" content="Tecnologias, carrera TI, Tecnlogias informacion, UTN" />
    <meta name="generator" content="HTML5, CSS3, asp, c#, javascript" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Titulo de la Pagina -->
    <title>TI/Alumnos</title>
    <!-- Enlances y/o Archivos Externos -->
    <link rel="icon" href="imagenes/icono.ico" />  
    <link rel="stylesheet" type="text/css" href="estilos/forms.css" />
    <style>
        header {
            background-image: url("imagenes/encabezado.jpg");
            background-repeat: no-repeat;
            background-size: 100%;
            height: 300px;
            opacity: 0.9;
        }
         
        .auto-style1 {
            width: 1109px;
        }
         
    </style>
    
</head>
<body>
     <!-- ======= Header ======= -->
    <header id="header">
        <img src="imagenes/LogoTIC.png" width="150" height="100"/>        
    </header><!-- ======= FIN Header ======= -->
    <nav class="menu">
        <ul>
            <li><a href="index.html">Inicio</a></li>
            <li><a href="Alumnos.aspx">Alta</a></li>
            <li><a href="">Baja</a></li>
            <li><a href="">Consulta General</a></li>
            <li><a href="ConsultaIndividual.aspx">Consulta Individual</a></li>
            <li><a href="">Mofificación</a></li>
        </ul>
    </nav>
    <br /><br />
    <section>
        <h1>Consulta General de Alumnos</h1>
    <form id="form1" runat="server" class="auto-style1">

        <asp:GridView ID="gridAlumnos" runat="server" DataSourceID="sqlAlumnos" AutoGenerateColumns="False" DataKeyNames="id_alumno" CellPadding="4" ForeColor="#333333" GridLines="None" class="tabla">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="id_alumno" HeaderText="Clave Alumno" InsertVisible="false" ReadOnly="true" SortExpression="id_alumno"/>
                <asp:BoundField DataField="nombres" HeaderText="Nombres" SortExpression="nombres" />
                <asp:BoundField DataField="ap_paterno" HeaderText="Apellido Paterno" SortExpression="ap_paterno" />
                <asp:BoundField DataField="ap_materno" HeaderText="Apellido Materno" SortExpression="ap_materno" />
                <asp:BoundField DataField="sexo" HeaderText="Sexo" SortExpression="sexo" />
                <asp:BoundField DataField="email" HeaderText="Correo Electrónico" SortExpression="email" />
                <asp:BoundField DataField="id_grupo" HeaderText="Grupo" SortExpression="id_grupo" />
                <asp:BoundField DataField="id_carrera" HeaderText="Carrera" SortExpression="id_carrera" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>


        <asp:SqlDataSource id="sqlAlumnos" runat="server" ConnectionString="<%$ ConnectionStrings:utConnectionString %>"
            SelectCommand="Select * From Alumnos">
        </asp:SqlDataSource>
    </form>
    </section>
</body>
</html>
