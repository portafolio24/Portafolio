﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModificacionAlumnno.aspx.cs" Inherits="SitioTI42.ModificacionAlumnno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <!-- Seccion Metadatos -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="sitio informativo de la carrera de IDGS" />
    <meta name="author" content="Emma Mariana Arias Camacho" />
    <meta name="keywords" content="Tecnologias, carrera TI, Tecnlogias informacion, UTN" />
    <meta name="generator" content="HTML5, CSS3, asp, c#, javascript" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Titulo de la Pagina -->
    <title>TI/Alumnos</title>
    <!-- Enlances y/o Archivos Externos -->
    <link rel="icon" href="imagenes/icono.ico" />  
    <link rel="stylesheet" type="text/css" href="estilos/forms.css" />
    <style>
        header {
            background-image: url("imagenes/encabezado.jpg");
            background-repeat: no-repeat;
            background-size: 100%;
            height: 300px;
            opacity: 0.9;
        }
         
        .auto-style1 {
            width: 1109px;
        }
         
    </style>
    
</head>
<body>
     <!-- ======= Header ======= -->
    <header id="header">
        <img src="imagenes/LogoTIC.png" width="150" height="100"/>        
    </header><!-- ======= FIN Header ======= -->
    <nav class="menu">
        <ul>
            <li style="width:20%"><a href="index.html">Inicio</a></li>
            <li style="width:20%"><a href="Alumnos.aspx">Alta</a></li>
            <li style="width:20%"><a href="BajaAlumno.aspx">Baja</a></li>
            <li style="width:20%"><a href="ConsultaGeneralAlumnos.aspx">Consulta</a></li>
            <li style="width:20%"><a href="ModificacionAlumnno.aspx">Mofificación</a></li>
        </ul>
    </nav>
    <br /><br />
     <form id="form1" runat="server">
         <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_alumno" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
             <AlternatingRowStyle BackColor="White" />
             <Columns>
                 <asp:BoundField DataField="id_alumno" HeaderText="id_alumno" InsertVisible="False" ReadOnly="True" SortExpression="id_alumno" />
                 <asp:BoundField DataField="nombres" HeaderText="Nombres" SortExpression="nombres" />
                 <asp:BoundField DataField="ap_paterno" HeaderText="Apellido Paterno" SortExpression="ap_paterno" />
                 <asp:BoundField DataField="ap_materno" HeaderText="Apellido Materno" SortExpression="ap_materno" />
                 <asp:BoundField DataField="sexo" HeaderText="Sexo" SortExpression="sexo" />
                 <asp:BoundField DataField="email" HeaderText="Correo" SortExpression="email" />
                 <asp:BoundField DataField="id_grupo" HeaderText="ID Grupo" SortExpression="id_grupo" />
                 <asp:BoundField DataField="id_carrera" HeaderText="ID Carrera" SortExpression="id_carrera" />
             </Columns>
             <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
             <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
             <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
             <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
             <SortedAscendingCellStyle BackColor="#FDF5AC" />
             <SortedAscendingHeaderStyle BackColor="#4D0000" />
             <SortedDescendingCellStyle BackColor="#FCF6C0" />
             <SortedDescendingHeaderStyle BackColor="#820000" />
         </asp:GridView>
         <br />
         <br />
          <table>
            <tr>
                <td>
                    <asp:Label ID="lbBaja" runat="server" Text="Ingrese el ID del alumno a modificar:" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbIdAlumno" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btBuscar" runat="server" Text="Buscar" OnClick="btBuscar_Click" />
                </td>
            </tr>

        </table>
         <br />
         <br />
         <table id="tbDatos" runat="server">
                <%--inicia primer renglon--%>
                <tr>
                    <td>
                        <asp:Label ID="lbNombres" runat="server" Text="Nombres"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tbNombres" runat="server" CssClass="textbox"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredNombres" runat="server" CssClass="validaciones" ControlToValidate="tbNombres" ErrorMessage="-El Nombre es campo obligatorio" Text="*"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularNombres" runat="server" CssClass="validaciones" ControlToValidate="tbNombres" ErrorMessage="-El  nombre solo puede contener letras y espacio en blanco" Text="*" ValidationExpression="^[a-zA-Z\s]*$"></asp:RegularExpressionValidator><br />
                    </td>
                </tr>
               <%-- inicia segundo renglon--%>
                <tr>
                    <td>
                        <asp:Label ID="lbPaterno" runat="server" Text="Apellido Paterno:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tbPaterno" runat="server" CssClass="textbox"></asp:TextBox>
                    </td>
                     <td>
                        <asp:RequiredFieldValidator ID="RequiredPaterno" runat="server" CssClass="validaciones" ControlToValidate="tbPaterno" ErrorMessage="-El apellido paterno es campo obligatorio" Text="*"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionPaterno" runat="server" CssClass="validaciones" ControlToValidate="tbPaterno" ErrorMessage="-El  apellido paterno solo puede contener letras y espacio en blanco" Text="*" ValidationExpression="^[a-zA-Z\s]*$"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:Label ID="lbMaterno" runat="server" Text="Apellido Materno:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tbMaterno" runat="server" CssClass="textbox"></asp:TextBox>
                    </td>
                     <td>
                        <asp:RequiredFieldValidator ID="RequiredMaterno" runat="server" CssClass="validaciones" ControlToValidate="tbMaterno" ErrorMessage="-El apellido materno es campo obligatorio" Text="*"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularMaterno" runat="server" CssClass="validaciones" ControlToValidate="tbMaterno" ErrorMessage="-El  apellido materno solo puede contener letras y espacio en blanco" Text="*" ValidationExpression="^[a-zA-Z\s]*$"></asp:RegularExpressionValidator>
                    </td>
                </tr> <%--finaliza segundo renglon--%>
               <%-- inicia tercer renglon--%>
                <tr>
                    <td>
                        <asp:Label ID="lbSexo" runat="server" Text="Sexo:"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblSexo" runat="server" CssClass="textbox">
                            <asp:ListItem Value="F">Femenino</asp:ListItem>
                            <asp:ListItem Value="M">Masculino</asp:ListItem>
                        </asp:RadioButtonList>
                        
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="lbCorreo" runat="server" Text="Correo Eletrónico:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="tbEmail" runat="server" CssClass="textbox"></asp:TextBox>
                    </td>
                     <td>
                        <asp:RequiredFieldValidator ID="RequiredEmail" runat="server" CssClass="validaciones" ControlToValidate="tbEmail" ErrorMessage="-El apellido email es campo obligatorio" Text="*"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="RegularEmail" runat="server" CssClass="validaciones" ControlToValidate="tbEmail" ErrorMessage="-Formato de correo no valido ejemplo@dominio.com" Text="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                 </tr>
                 <%--inicia cuarto renglon--%>
                <tr>
                    <td>
                        <asp:Label ID="lbCarrera" runat="server" Text="Carrera:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCarrera" runat="server" CssClass="textbox" DataSourceID="sqlCarrera" DataValueField="id_carrera" DataTextField="carrera" ></asp:DropDownList>
                    </td>
                    <td></td>
                    <td>
                        <asp:Label ID="lbGrupo" runat="server" Text="Grupo:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGrupo" runat="server" CssClass="textbox" DataSourceID="sqlGrupo" DataValueField="id_grupo" DataTextField="Grupo"></asp:DropDownList>
                    </td>
                </tr> 
            </table>
         <table id="tbBotones" runat="server">
             <tr>
                 <td>
                     <asp:ImageButton ID="btGuardar" runat="server" ImageUrl="~/imagenes/guardar.png" OnClick="btGuardar_Click" />
                 </td>
                 <td>
                     <asp:ImageButton ID="btCancelar" runat="server" ImageUrl="~/imagenes/cancelar.png" />
                 </td>
             </tr>
         </table>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:utConnectionString %>" SelectCommand="SELECT * FROM [Alumnos]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sqlCarrera" runat="server" ConnectionString="<%$ ConnectionStrings:utConnectionString %>" SelectCommand="SELECT * FROM [Carreras]"></asp:SqlDataSource>
     <asp:SqlDataSource ID="sqlGrupo" runat="server" ConnectionString="<%$ ConnectionStrings:utConnectionString %>" SelectCommand="SELECT * FROM [Grupos]"></asp:SqlDataSource>
     </form>
</body>
</html>
