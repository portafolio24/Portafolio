﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SitioTI42
{
    public partial class Autenticacion : System.Web.UI.Page
    {
        //Declarar variable usuarios manera global
        public static string Usuario;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btIngresar_Click(object sender, EventArgs e)
        {
            if (tbUsuario.Text != "" && tbPass.Text != "") {
                string eacUsuario, eacPass;
                eacUsuario = tbUsuario.Text;
                eacPass = tbPass.Text;

                //Crear objeto para acceder al DataSet
                utTableAdapters.UsuariosTableAdapter talogin = new utTableAdapters.UsuariosTableAdapter();

                //Crear el objeto para ingresar a la tabla de usuarios
                ut.UsuariosDataTable dtlogin = talogin.GetData(eacUsuario, eacPass);

                int cantidad = dtlogin.Count; //Cantidad de registros encontrados

                if (cantidad <= 0) {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "javascript:alert('Usuario y/o contraseña incorrectos');", true);
                }
                else {
                    //Asignar variable de sesión para el usuario logueado

                    Usuario = dtlogin[0].usuario;
                    Response.Redirect("Alumnos.aspx");

                }

            }
            else {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "javascript:alert('Se necesita ingresar información al campo Usuario y Constraseña');", true);
            }
        }
    }
}