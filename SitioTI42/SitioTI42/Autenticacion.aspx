﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Autenticacion.aspx.cs" Inherits="SitioTI42.Autenticacion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content="sitio informativo de la carrera de IDGS" />
    <meta name="author" content="Emma Mariana Arias Camacho" />
    <meta name="keywords" content="Tecnologias, carrera TI, Tecnlogias informacion, UTN" />
    <meta name="generator" content="HTML5, CSS3, asp, c#, javascript" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Titulo de la Pagina -->
    <title>TI/Alumnos</title>
    <!-- Enlances y/o Archivos Externos -->
    <link rel="icon" href="imagenes/icono.ico" />  
    <link rel="stylesheet" type="text/css" href="estilos/forms.css" />
    <style>
        header {
            background-image: url("imagenes/encabezado.jpg");
            background-repeat: no-repeat;
            background-size: 100%;
            height: 300px;
            opacity: 0.9;
        }
         
        .auto-style1 {
            width: 1109px;
        }

        .botonI{
            height: 30px;
        }
         
    </style>
    
</head>
<body>
     <!-- ======= Header ======= -->
    <header id="header">
        <img src="imagenes/LogoTIC.png" width="150" height="100"/>        
    </header><!-- ======= FIN Header ======= -->
    <br /><br />
    <h1>Ingreso al Sistema Alumnos</h1>
    <form id="form1" runat="server">
        <table style="background-color:white;border-radius:5px 5px">
            <tr>
                <td>
                    <asp:Label ID="lbUsuario" runat="server" Text="Usuario:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbUsuario" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbPass" runat="server" Text="Contraseña:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbPass" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btIngresar" runat="server" Text="Ingresar" CssClass="boton" Height="35px" Width="85px" OnClick="btIngresar_Click"></asp:Button>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
