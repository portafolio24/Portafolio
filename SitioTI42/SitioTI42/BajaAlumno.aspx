﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BajaAlumno.aspx.cs" Inherits="SitioTI42.BajaAlumno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
&nbsp;<!-- Seccion Metadatos --><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta name="description" content="sitio informativo de la carrera de IDGS" /><meta name="author" content="Emma Mariana Arias Camacho" /><meta name="keywords" content="Tecnologias, carrera TI, Tecnlogias informacion, UTN" /><meta name="generator" content="HTML5, CSS3, asp, c#, javascript" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><!-- Titulo de la Pagina --><title>TI/Alumnos</title><!-- Enlances y/o Archivos Externos --><link rel="icon" href="imagenes/icono.ico" /><link rel="stylesheet" type="text/css" href="estilos/forms.css" /><style>
        header {
            background-image: url("imagenes/encabezado.jpg");
            background-repeat: no-repeat;
            background-size: 100%;
            height: 300px;
            opacity: 0.9;
        }
         
        </style></head><body><!-- ======= Header ======= --><header id="header">
        <img src="imagenes/LogoTIC.png" width="150" height="100"/>        
    </header><!-- ======= FIN Header ======= -->
    <nav class="menu">
        <ul>
            <li><a href="index.html">Inicio</a></li>
            <li><a href="Alumnos.aspx">Alta</a></li>
            <li><a href="">Baja</a></li>
            <li><a href="ConsultaGeneralAlumnos.aspx">Consulta General</a></li>
            <li><a href="">Consulta Individual</a></li>
            <li><a href="">Mofificación</a></li>
        </ul>
    </nav>
    <br /><br />
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_alumno" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="id_alumno" HeaderText=" ID Alumno " InsertVisible="False" ReadOnly="True" SortExpression="id_alumno" />
                    <asp:BoundField DataField="nombres" HeaderText=" Nombres " SortExpression="nombres" />
                    <asp:BoundField DataField="ap_paterno" HeaderText=" Apellido Paterno " SortExpression="ap_paterno" />
                    <asp:BoundField DataField="ap_materno" HeaderText=" Apellido Materno " SortExpression="ap_materno" />
                    <asp:BoundField DataField="sexo" HeaderText=" Sexo " SortExpression="sexo" />
                    <asp:BoundField DataField="email" HeaderText=" Correo " SortExpression="email" />
                    <asp:BoundField DataField="id_grupo" HeaderText=" ID Grupo " SortExpression="id_grupo" />
                    <asp:BoundField DataField="id_carrera" HeaderText=" ID Carrera " SortExpression="id_carrera" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:utConnectionString %>" SelectCommand="SELECT * FROM [Alumnos]"></asp:SqlDataSource>
        <br />
        <br />
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbBaja" runat="server" Text="Ingrese el ID del alumno a eliminar:" Font-Bold="true"></asp:Label>
                    <asp:TextBox ID="tbBaja" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btEliminar" runat="server" Text="Eliminar" CssClass="boton" OnClick="btEliminar_Click" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
